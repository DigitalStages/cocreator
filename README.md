# CoCreator

### Project description:

A deep learning algorithm builds 3D printable sculptures from data that visitors to the exhibition create through their own movements. Visitors can view these sculptures on a monitor and make changes with the help of an evolutionary algorithm.

![Alt text](/img/PHSculpt_Mockup.png)

A selection of sculptures will later be exhibited as printed sculptures.

![Alt text](/img/PHSculpt_Mockup2.png)
